/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fusegaming.bugtracker.web.rest.vm;
